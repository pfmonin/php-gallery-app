<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");}?>
<?php 

if(empty($_GET['id'])){
    redirect("photos.php");
}

$comments = Comment::find_the_comments($_GET['id']);




?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- top navigation -->
        <?php include("includes/top_nav.php") ?>
        <!-- sidenav -->
        <?php include("includes/sidenav.php") ?>
        </nav>

        <div id="page-wrapper">
            <!-- admin content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Comments photo
                            <small>Subheading</small> 
                        </h1>
                        <h2 class="text-right" style="font-size: 1.5em">
                            <a href="add_user.php" class="btn btn-primary"><i class="fa fa-plus fa-1x" ></i></a>
                        </h2>
                        
                        <div class="col-md-12">                        
                            <table class="table table-hover">                            
                                <thead>
                                    <tr>                                        
                                        <th>Id</th>
                                        <th>author</th>
                                        <th>Comment</th>                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>

                                <?php foreach($comments as $comment) :  ?>
                                
                                    <tr>
                                        <td> <?php echo $comment->id;  ?> </td>                                        
                                        <td> <?php echo $comment->author;  ?> </td>                                        
                                        <td> <?php echo $comment->body;  ?> </td>                                        
                                        <td>
                                           <div class="action_link">
                                                <a class="delete_link" href="delete_comment.php?id=<?php echo $comment->id; ?>"><i class="fa fa-trash "></i></a>
                                                <a href="edit_comment.php?id=<?php echo $comment->id; ?>"><i class="fa fa-edit "></i></a>
                                                <a href="#"><i class="fa fa-eye fa-2x"></i></a>
                                            </div> 
                                        </td>
                                    </tr>

                                <?php endforeach;  ?>
                                
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
                <!-- /.row -->
            </div>
<!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>