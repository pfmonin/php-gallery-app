<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");}?>


        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- top navigation -->
        <?php include("includes/top_nav.php") ?>
        <!-- sidenav -->
        <?php include("includes/sidenav.php") ?>
        </nav>

        <div id="page-wrapper">
            <!-- admin content -->
            <?php include("includes/admin_content.php") ?>
        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>