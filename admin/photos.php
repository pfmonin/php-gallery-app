<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");}?>
<?php $photos = Photo::find_all();  ?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- top navigation -->
        <?php include("includes/top_nav.php") ?>
        <!-- sidenav -->
        <?php include("includes/sidenav.php") ?>
        </nav>

        <div id="page-wrapper">
            <!-- admin content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Photos
                            <small>Subheading</small>
                        </h1>
                        <p class="bg-success"> <?php echo $message; ?> </p>
                        <div class="col-md-12">                        
                            <table class="table table-hover">                            
                                <thead>
                                    <tr>
                                        <th>Photo</th>
                                        <th>Id</th>
                                        <th>Filename</th>
                                        <th>Title</th>
                                        <th>Size</th>
                                        <th>Comment</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <?php foreach($photos as $photo) :  ?>
                                
                                    <tr>
                                        <td> <img src="<?php echo $photo->picture_path();?>" style="width: 150px;heigth:auto;" alt=""></td>
                                        <td> <?php echo $photo->id;  ?> </td>
                                        <td> <?php echo $photo->filename;  ?> </td>
                                        <td> <?php echo $photo->title  ?> </td>
                                        <td> <?php echo $photo->size;  ?> </td>
                                        <td>                                         
                                        <?php 
                                            $comments = Comment::find_the_comments($photo->id);
                                            echo count($comments); 
                                        ?>
                                        </td>
                                        <td>
                                            <div class="pictures_link">
                                                <a class="delete_link" href="delete_photo.php?id=<?php echo $photo->id; ?>"><i class="fa fa-trash fa-2x"></i></a>
                                                <a href="edit_photo.php?id=<?php echo $photo->id; ?>"><i class="fa fa-edit fa-2x"></i></a>
                                                <a href="../photo.php?id=<?php echo $photo->id ?>"><i class="fa fa-eye fa-2x"></i></a>
                                                <a href="comments_photo.php?id=<?php echo $photo->id ?>"><i class="fa fa-comments"></i></a>
                                            </div>
                                        </td>
                                    </tr>

                                <?php endforeach;  ?>
                                
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
                <!-- /.row -->
            </div>
<!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>