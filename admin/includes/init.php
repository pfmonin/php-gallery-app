<?php 

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR); 

define('SITE_ROOT', 'C:' . DS . 'wamp64' . DS . 'www' . DS . 'php' . DS . 'php_photoGallery' . DS . 'photoGalleryApp' . DS . 'gallery'); 

defined('INCLUDES_PATH') ? null : define('INCLUDES_PATH', SITE_ROOT . DS . 'admin' . DS . 'includes');

require_once("new_config.php");
require_once("function.php");
require_once("database.php");
require_once("db_object.php");
require_once("session.php");
require_once("user.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . "photo.php");
require_once("comment.php");
require_once("paginate.php");



?>

