<?php include("includes/header.php"); ?>
<?php

$page = !empty($_GET['page']) ? (int)$_GET['page'] :1;

$item_per_page = 9;

$item_total_count = Photo::count_all();

$photos = Photo::find_all(); 

$paginate = new Paginate($page,$item_per_page, $item_total_count);

$sql = "SELECT * FROM photos ";
$sql .= " LIMIT {$item_per_page} ";
$sql .= " OFFSET {$paginate->offset()} ";
$photos = Photo::find_by_query($sql);

?>

<div class="row">

    <div class="col-md-12">
        <div class="thumbnails row">

    <?php foreach($photos as $photo) : ?>         
        
        <div class="col-xs-6 col-md-4">
        
            <a href="photo.php?id=<?php echo $photo->id; ?>" class="thumbnail">
                <img src="admin/<?php echo $photo->picture_path(); ?>" alt="">
            </a>
        
        </div>

    <?php endforeach ?>
    </div>

    <div class="row">
    
        <ul class="pagination">

            <?php

                if($paginate->page_total() > 1){ 
                    if($paginate->has_next()){
                        echo "<li class='next'><a href='index.php?page={$paginate->next()}'>Next</a></li>";
                    }
                }



                for($i=1; $i<=$paginate->page_total(); $i++){
                    if($i == $paginate->current_page){

                        echo "<li><a href='index.php?page={$i}' class='active' style='color: lightblue;margin: 0 5px;'> {$i} </a></li>";
                    }else{
                        echo "<li><a href='index.php?page={$i}' style='margin: 0 5px;'> {$i} </a></li>";

                    }
                }





                if($paginate->page_total() > 1){ 
                    if($paginate->has_previous()){
                        echo "<li class='previous'><a href='index.php?page={$paginate->previous()}'>Previous</a></li>";
                    }
                }

            ?>            
        </ul>
    
    </div>






</div>




           
        <?php include("includes/footer.php"); ?>
