<?php include("includes/header.php"); ?>

<?php
require_once("admin/includes/init.php");
require_once("admin/includes/photo.php");
// require_once("admin/includes/comment.php");



if(empty($_GET['id'])){
    redirect("index.php");
}

$photo = Photo::find_by_id($_GET['id']);


if(isset($_POST['submit'])){
    $author = trim($_POST['author']);
    $body = trim($_POST['body']);

    $new_comment = Comment::create_comment($photo->id, $author, $body);

    if($new_comment &&  $new_comment->save()){

        redirect("photo.php?id={$photo->id}");
    }else{
        $message = "There was some problems saving.";
    }

   
}else{

    $author = " ";
    $body = " ";
}

$comments = Comment::find_the_comments($photo->id);





?>





    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-12">

                <!-- Blog Post -->

                <!-- Title -->
                <h1>Blog Post Title</h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#">Pierre-Francois Monin</a>
                </p>

                <hr>

                <h1> <?php echo $photo->title; ?> </h1>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on August 24, 2013 at 9:00 PM</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="admin/<?php echo $photo->picture_path(); ?>" alt="">
               
                <hr>

                <p> <?php echo $photo->description; ?>  </p>



                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post" >
                    
                        <div class="form-group">                            
                            <input type="text" name="author" id="" class="form-control" placeholder="Author">
                        </div>

                        <div class="form-group">
                            <textarea name="body" class="form-control" rows="3" placeholder="Your message here..."></textarea>
                        </div>

                        <input type="submit"  name="submit" class="btn btn-primary">
                    </form>
                </div>

                <hr>

                <?php foreach($comments as $comment) :  ?>

                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="https://picsum.photos/64" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $comment->author ?></h4>
                        </div>
                        <p> <?php echo $comment->body ?> </p>
                    </div>

                <?php endforeach; ?>

                

            </div>

           

            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center">Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
