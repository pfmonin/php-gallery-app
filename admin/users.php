<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");}?>
<?php $users = User::find_all();  ?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- top navigation -->
        <?php include("includes/top_nav.php") ?>
        <!-- sidenav -->
        <?php include("includes/sidenav.php") ?>
        </nav>

        <div id="page-wrapper">
            <!-- admin content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">                    
                        <h1 class="page-header">
                           Users 
                            <small>Subheading</small> 
                        </h1>
                        <p class="bg-success"> <?php echo $message; ?> </p>
                        <h2 class="text-right" style="font-size: 1.5em">
                            <a href="add_user.php" class="btn btn-primary"><i class="fa fa-plus fa-1x" ></i></a>
                        </h2>
                        
                        <div class="col-md-12">                        
                            <table class="table table-hover">                            
                                <thead>
                                    <tr>                                        
                                        <th>Id</th>
                                        <th>Photo</th>
                                        <th>Username</th>                                        
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <?php foreach($users as $user) :  ?>
                                
                                    <tr>
                                        <td> <?php echo $user->id;  ?> </td>
                                        <td> <img src="<?php echo $user->image_path_placeholder(); ?>" class="thumbnail" alt=""></td>
                                        <td> <?php echo $user->username;  ?> </td>                                        
                                        <td> <?php echo $user->first_name;  ?> </td>
                                        <td> <?php echo $user->last_name;  ?> </td>
                                        <td>
                                           <div class="action_link">
                                                <a class="delete_link" href="delete_user.php?id=<?php echo $user->id; ?>"><i class="fa fa-trash "></i></a>
                                                <a href="edit_user.php?id=<?php echo $user->id; ?>"><i class="fa fa-edit "></i></a>
                                                <a href="#"><i class="fa fa-eye fa-2x"></i></a>
                                            </div> 
                                        </td>
                                    </tr>

                                <?php endforeach;  ?>
                                
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
                <!-- /.row -->
            </div>
<!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>