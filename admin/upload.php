<?php include("includes/header.php"); ?>

<?php if(!$session->is_signed_in()) {redirect("login.php");}?>

<?php

$message = "";

if(isset($_FILES['file'])){
    
    $photo = new Photo();
    $photo->title = $_POST['title'];
    $photo->set_file($_FILES['file']);

    if($photo->save()){
        $message = "Photo updated succefully";
    }else{
        $message = join("<br>", $photo->errors);
    }
}

?>




        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- top navigation -->
        <?php include("includes/top_nav.php") ?>
        <!-- sidenav -->
        <?php include("includes/sidenav.php") ?>
        </nav>

        <div id="page-wrapper">
            <!-- admin content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Upload
                            <small>Subheading</small>
                        </h1>         
                        
                       <div class="col-md-6">

                            <?php  
                            echo $message;                            
                            ?> 

                       
                            <div class="row">
                            
                                <form action="upload.php" method="post" enctype="multipart/form-data">
                                    
                                    <div class="form-group">
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="file" >
                                    </div>

                                    <input type="submit" name="submit" class="btn btn-primary">
                                
                                </form>
                            
                            </div> 

                            <div class="row">
                            
                                <div class="col-lg-12">
                                
                                    <form action="upload.php" class="dropzone" id="my-awesome-dropzone"></form>
                                
                                </div>
                            
                            </div>

                       </div>

                    </div>
                </div>
                <!-- /.row -->
            </div>
<!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>