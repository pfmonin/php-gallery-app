<?php


class Db_object{

    protected static $db_table = "users"; 
    
    public $errors = array();
    public $upload_errors = array(

        UPLOAD_ERR_OK           => "There is no error",
        UPLOAD_ERR_INI_SIZE     =>"The uploaded file exceeds the upload_max_filesize directive",
        UPLOAD_ERR_FORM_SIZE    =>"The uploaded file exceeds the max_file_size directive",
        UPLOAD_ERR_PARTIAL      =>"The uploaded file was only partially uploaded",
        UPLOAD_ERR_NO_FILE      =>"No file was uploaded",
        UPLOAD_ERR_NO_TMP_DIR   =>"Missing a temporary folder", 
        UPLOAD_ERR_CANT_WRITE   =>"Failed to write file to disk", 
        UPLOAD_ERR_EXTENSION    =>"A PHP extension stopped the file upload"

    );  
   


    // --- Instantiation & has the attribute --- //

    public static function instantation($the_record){

        $calling_class = get_called_class();

        $the_object = new $calling_class;
        // $the_object->id = $found_user['id'];
        // $the_object->username = $found_user['username'];
        // $the_object->password = $found_user['password'];
        // $the_object->first_name = $found_user['first_name'];
        // $the_object->last_name = $found_user['last_name'];
        // all this above can be done with a foreach, loop see below

        foreach($the_record as $the_attribute=>$value){
            if($the_object->has_the_attribute($the_attribute)){
                $the_object->$the_attribute = $value;
            }
        }        

        return $the_object;
    }


    private function has_the_attribute($the_attribute){
        $object_properties = get_object_vars($this);
        return array_key_exists( $the_attribute, $object_properties );

    }




    // --- Properties & clean_properties --- //

    protected function properties(){
        $properties = array();
        foreach(static::$db_table_fields as $db_field){
            if(property_exists($this, $db_field)){
                $properties[$db_field] = $this->$db_field;
            }
        }
        return $properties;
    }

    protected function clean_properties(){
        global $database;

        $clean_properties = array();

        foreach($this->properties() as $key=>$value){
            $clean_properties[$key] = $database->escape_string($value);
        }
        return $clean_properties;
    }




     // --- query builder --- //

     public static function find_by_query($sql){       // permet de généraliser la query +- egale a l'entity manager get query
        global $datablase;
        $result_set = $datablase->query($sql);
        $the_object_array = array();        // added rigth after the instantiation methods, to be able to loop into the object ot get all the information needed

        while($row = mysqli_fetch_array($result_set)){
            $the_object_array[] = static::instantation($row);
        }

        return $the_object_array;
    }



    // --- Find all --- //

    public static function find_all(){       
       
        return static::find_by_query("SELECT * FROM " . static::$db_table ." ");  
    }

    // --- Find by ID --- //

    public static function find_by_id($id){
        
        $the_result_array = static::find_by_query("SELECT * FROM " . static::$db_table . " WHERE id=$id LIMIT 1");
        
         if(!empty($the_result_array)){
             $first_item = array_shift($the_result_array);
             return $first_item;
         }else{
             return false;
         }
        
    }
    


    // --- CRUD --- // 

    public function save(){
        return isset($this->id) ? $this->update() : $this->create();   // on cherche si l'ID de l'user existe puis suivant le cas on ajoute ou modifie
    }
        
    public function create(){
        global $database;

        // $sql = "INSERT INTO " .self::$db_table. " (username, password, first_name, last_name)";
        // $sql .= "VALUES ('";
        // $sql .= $database->escape_string($this->username) . "', '";
        // $sql .= $database->escape_string($this->password) . "', '";
        // $sql .= $database->escape_string($this->first_name) . "', '";
        // $sql .= $database->escape_string($this->last_name) . "')";

        // here is another way to make this create function reusable for any entities

        $properties = $this->clean_properties();

        if(array_key_exists('id', $properties)){
            unset($properties['id']);
        }

        $sql = "INSERT INTO " . static::$db_table . " ( " . implode( ",", array_keys($properties)) . " )";
        $sql .= " VALUES ('". implode( "','", array_values($properties)) ."')";


        if($database->query($sql)){
            $this->id = $database->the_insert_id();
            return true;
        }else{
            return false;
        }        
    }

    public function update(){
        global $database;

        // $sql = "UPDATE "  . self::$db_table . " SET ";        
        // $sql .= "username= ' " . $database->escape_string($this->username) . "', ";
        // $sql .= "password= ' " . $database->escape_string($this->password) . "', ";
        // $sql .= "first_name= ' " . $database->escape_string($this->first_name) . "', ";
        // $sql .= "last_name= ' " . $database->escape_string($this->last_name) . "' ";
        // $sql .= " WHERE id= " . $database->escape_string($this->id);


        // here is another way to make this update function reusable for any entities
        $properties = $this->clean_properties();

        $properties_pairs = array();
        foreach($properties as $key => $value){
            $properties_pairs[] = " {$key}='{$value}'";
        }

        $sql = "UPDATE "  . static::$db_table . " SET ";        
        $sql .= implode(", ", $properties_pairs);
        $sql .= " WHERE id= " . $database->escape_string($this->id);

        $database->query($sql);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false; //another way to make a If statement 
    }

    public function delete(){
        global $database;

        $sql = "DELETE FROM "  . static::$db_table . " ";
        $sql .=" WHERE id= " . $database->escape_string($this->id);
        $sql .= " LIMIT 1";

        $database->query($sql);
        return (mysqli_affected_rows($database->connection) == 1) ? true : false; //another way to make a If statement 

    }

    public static function count_all(){

        global $database;

        $sql = "SELECT COUNT(*) FROM " . static::$db_table;
        $result_set = $database->query($sql); 
        $row = mysqli_fetch_array($result_set);
        
        return array_shift($row);
    }

    

    














}


?>