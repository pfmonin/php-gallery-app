<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Admin Content
                            <small>Dashboard</small>
                        </h1> 


                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-users fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge"><?php echo $session->count; ?> </div>
                                               
                                                <div>New Views</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">                                            
                                        <span class="pull-left">View Details</span> 
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span> 
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-photo fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge"> <?php echo Photo::count_all(); ?> </div>
                                                <div>Photos</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left">Total Photos in Gallery</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-user fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge"><?php echo User::count_all(); ?></div>

                                                <div>Users</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left">Total Users</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-red">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-support fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge"><?php echo Comment::count_all(); ?></div>
                                                <div>Comments</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left">Total Comments</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>


                        </div> <!--First Row-->                       
                        
                        <div class="row">
                            <div id="chart_div"></div>                        
                        </div>

            </div>
            <!-- /.container-fluid -->



            <?php 

                        // $result_set = User::find_all_users();
                        // while($row = mysqli_fetch_array($result_set)){
                        //     echo $row['username'] . "<br>";
                        // } 

                        // $users = User::find_all();

                        // foreach($users as $user){
                        //     echo $user->username . "<br>";
                        // }         
                        
                        // $photos = Photo::find_all();

                        // foreach($photos as $photo){
                        //     echo $photo->title . "<br>";
                        // }  
                        
                        
                        // $photo = Photo::find_by_id(6);
                        // echo $photo->title . "<br>";                        
                        
                        
                        //create user
                        // $photo = new Photo();
                        // $photo->title = "photo_2";
                        // $photo->description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. In est maiores enim assumenda distinctio reiciendis! Accusamus delectus dolore nisi ea voluptatibus totam doloremque eius! Cupiditate praesentium tenetur id reiciendis laborum?";
                        // $photo->filename = "photo_2.jpg";
                        // $photo->type = "image";
                        // $photo->size = "11";
                        // $photo->create();

                        // $user->create();

                        //update user
                        // $user = User::find_by_id(4);
                        // $user->username = "user_4";
                        // $user->update();

                        // Delete user
                        // $user = User::find_by_id(6);
                        // $user->delete();

                        
                        
                        ?>
                        
